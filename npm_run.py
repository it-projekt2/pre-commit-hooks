import subprocess
import argparse
from typing import Sequence


def main(argv: Sequence[str] | None = None) -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument('--dir', dest='dir', default='.')
    parser.add_argument('--run', dest='run', default='lint')
    parser.add_argument('--no-files', dest='no_files', default=False, action='store_true')
    parser.add_argument('filenames', nargs='*')
    args = parser.parse_args(argv)

    files = args.filenames

    if not _did_file_in_dir_change(files, args.dir):
        return 0

    command = f"npm run {args.run}"

    if not args.no_files:
        relative_paths = map(lambda f: f.replace(args.dir, ""), files)
        files_string = " ".join(relative_paths)
        command = f"{command} {files_string}"

    print(command)
    cmd = subprocess.Popen(command, cwd=args.dir, shell=True)
    cmd.communicate()
    return cmd.returncode


def _did_file_in_dir_change(files, directory):
    return any([directory in filename for filename in files])


if __name__ == '__main__':
    raise SystemExit(main())
